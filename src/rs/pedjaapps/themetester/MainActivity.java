package rs.pedjaapps.themetester;

import java.util.Locale;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import javax.security.auth.SubjectDomainCombiner;
import android.view.SubMenu;
import android.widget.ListView;
import android.widget.Toast;
import android.view.WindowManager;

public class MainActivity extends FragmentActivity implements
		ActionBar.TabListener {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
		setContentView(R.layout.activity_main);

		// Set up the action bar.
		actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
	//	for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab().setText("Widgets").setTabListener(this));
	    	actionBar.addTab(actionBar.newTab().setText("List View").setTabListener(this));
	    	actionBar.addTab(actionBar.newTab().setText("Other").setTabListener(this));
			
			
			
	//	}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		menu.add(0,0,0,"Settings").setIcon(android.R.drawable.ic_menu_preferences).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		menu.add(0,0,0,"Settings").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		menu.add(0,0,0,"Settings").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		SubMenu sub = menu.addSubMenu(1, 1, 3, "Submenu");
		sub.add(1, 2, 0, "Option 1");
		sub.add(1, 3, 1, "Option 2");
		sub.add(1, 4, 2, "Option 3");
		sub.add(1, 5, 3, "Option 4");
		sub.add(1, 6, 4, "Option 5");
		sub.add(1, 7, 5, "Option 6");
		SubMenu sub2 = sub.addSubMenu(1, 8, 6, "Submenu 2");
		sub2.add(1, 2, 0, "Option 1");
		sub2.add(1, 3, 1, "Option 2");
		sub2.add(1, 4, 2, "Option 3");
		return true;
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment = new DummySectionFragment();
			Bundle args = new Bundle();
			args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position);
			Toast.makeText(MainActivity.this, ""+position, Toast.LENGTH_LONG);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class DummySectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public DummySectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			/*View rootView = inflater.inflate(R.layout.fragment_main_dummy,
					container, false);
			TextView dummyTextView = (TextView) rootView
					.findViewById(R.id.section_label);
			dummyTextView.setText(Integer.toString(getArguments().getInt(
					ARG_SECTION_NUMBER)));*/
			//container.removeAllViews();
			if (getArguments().getInt(ARG_SECTION_NUMBER) == 0) {
				View rootView = inflater.inflate(R.layout.fragment_main_widgets,
								container, false);
				return rootView;
			} else if (getArguments().getInt(ARG_SECTION_NUMBER) == 1) {
				View rootView = inflater.inflate(R.layout.fragment_main_lv,
								container, false);
				return rootView;
			} else if (getArguments().getInt(ARG_SECTION_NUMBER) == 2) {
			View rootView = inflater.inflate(R.layout.fragment_main_other,
								container, false);
				return rootView;
			}
			else{
				return null;
			}
		//	return null;
		}
	}
	
/*	public static void Widgets(LayoutInflater inflater, ViewGroup container) {
		inflater.inflate(R.layout.fragment_main_dummy, container, false);
	}

	public static void mListView(LayoutInflater inflater, ViewGroup container) {
		inflater.inflate(R.layout.fragment_main_dummy, container, false);
	}
	
	public static void Other(LayoutInflater inflater, ViewGroup container) {
		inflater.inflate(R.layout.fragment_main_dummy, container, false);
	}*/
}
